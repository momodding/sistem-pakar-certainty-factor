<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>SiPakar Penyakit Sapi</title>
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link rel="stylesheet" href="assets/css/bootstrap/bootstrap.min.css">
  <link rel="stylesheet" href="assets/css/iCheck/all.css">
  <link rel="stylesheet" href="assets/css/home/css/style.css">
</head>
<body>
  <?php include 'menu.php'; ?>
  <div class="container main-box">
    <div class="card card-1">
      <div class="page-header">
        <h2>Hasil Konsultasi</h2>
      </div>
        <div id="diagnosa">
          <?php
              require 'setting/koneksi.php';
                if (isset($_POST['tampil'])) {
                  $gejala = $_POST['gejala'];

                  /*print_r($gejala);

                  echo "<br>";

                  for ($i=0; $i < count($gejala) ; $i++) {
                    //echo $gejala[$i].",";
                    if ($i == count($gejala)-1) {
                      echo $gejala[$i];
                    }else {
                      echo $gejala[$i].",";
                    }
                  }

                  echo "<br>";

                  foreach ($gejala as $key) {
                    echo $key."<br>";
                  }*/

              ?>
              <?php
                $sql = "SELECT id_penyakit FROM cf_table WHERE id_gejala IN(".implode(',',$gejala).") GROUP BY id_penyakit ORDER BY id_penyakit";

                //echo $sql;

                echo "<br>";

                $result = $conn->query($sql);
                if($result->num_rows > 0){
                  while($row=$result->fetch_assoc()){
                    $penyakit[] =  "'".$row['id_penyakit']."'";
                  }
                }
                print_r("Jumlah penyakit = ".count($penyakit));

                echo "<br>";


                for ($i=0; $i < count($penyakit); $i++) {
                  unset($gejalanya);
                  unset($mb);
                  unset($md);
                  $sql = "SELECT id_penyakit, mb, md, id_gejala FROM cf_table WHERE id_gejala IN(".implode(',',$gejala).") AND id_penyakit = ".$penyakit[$i];
                  $result = $conn->query($sql);
                  $k = $i+1;
                  echo "<br>";
                  echo "penyakit ke-".$k."(".$penyakit[$i].")";
                  echo "<br>";
                  echo "==========================";
                  if($result->num_rows > 0){
                    while($row=$result->fetch_assoc()){
                      $gejalanya[] =  $row['id_gejala'];
                      $mb[] = $row['mb'];
                      $md[] = $row['md'];
                    }
                    echo "<br>";
                    print_r("jml gejala = ".count($gejalanya));
                    echo "<br>";
                    if (count($gejalanya) == '1') {
                      $mbsatu = $mb[0];
                      $mdsatu = $md[0];
                      $cfsementara[$penyakit[$i]] = $mbsatu - $mdsatu;
                    }else {
                      for ($a=0; $a < count($gejalanya); $a++) {
                        //echo $mb[$i]."<br>";
                        $ke = $a+1;
                        echo "gejala ke-".$ke;
                        echo "<br>";
                        echo "==========================";
                        echo "<br>";
                        if ($a == 0) {
                          $mblama = $mb[$a];
                          echo "mblama = ".$mblama;
                          echo "<br>";
                          $mdlama = $md[$a];
                          echo "mdlama = ".$mdlama;
                          echo "<br>";
                          echo "==========================";
                          echo "<br>";
                        } else {
                          $mbbaru = $mb[$a];
                          echo "mbbaru = ".$mbbaru;
                          echo "<br>";
                          $mdbaru = $md[$a];
                          echo "mdbaru = ".$mdbaru;
                          echo "<br>";
                          $mbsementara = $mblama + ($mbbaru*(1-$mblama));
                          echo "mbsementara = ".$mbsementara;
                          echo "<br>";
                          $mdsementara = $mdlama + ($mdbaru*(1-$mdlama));
                          echo "mdsementara = ".$mdsementara;
                          echo "<br>";
                          $mblama = $mbsementara;
                          $mdlama = $mdsementara;
                          echo "==========================";
                          echo "<br>";
                        }
                      }
                      $mbfinal = $mbsementara;
                      $mdfinal = $mdsementara;
                      $cfsementara[$penyakit[$i]] = $mbfinal - $mdfinal;
                    }
                    echo "nilai cf = ".$cfsementara[$penyakit[$i]];
                    echo "<br>";
                  }
                }
                echo "<br>";
                $solusi = max($cfsementara);
                echo "Nilai cf terbesar = ".$solusi;
                echo "<br>";
                $solusikey = array_search($solusi, $cfsementara);
                echo "Penyakit yang diderita = ".$solusikey;

              ?>

        <?php } ?>
        </div>
      </div>
    </div>
  </div>





  <?php include 'footer.php'; ?>

  <!-- jQuery 3 -->
  <script src="assets/js/jQuery/jquery.min.js"></script>
  <!-- Bootstrap 3.3.7 -->
  <script src="assets/js/bootstrap/bootstrap.min.js"></script>
  <!-- Select2 -->
  <script src="assets/js/iCheck/icheck.min.js"></script>
  <!-- Sweetalert -->
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
  <!-- InputMask -->
  <script src="assets/js/input-mask/jquery.inputmask.js"></script>
  <script src="assets/js/input-mask/jquery.inputmask.date.extensions.js"></script>
  <script src="assets/js/input-mask/jquery.inputmask.extensions.js"></script>
  <!-- FastClick -->
  <script src="assets/js/fastclick/fastclick.js"></script>
  <!-- AdminLTE App -->
  <script src="assets/js/adminlte/adminlte.min.js"></script>
  <!-- Sparkline -->
  <script src="assets/js/jquery-sparkline/jquery.sparkline.min.js"></script>
  <!-- jvectormap  -->
  <script src="assets/js/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
  <script src="assets/js/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
  <!-- SlimScroll -->
  <script src="assets/js/jquery-slimscroll/jquery.slimscroll.min.js"></script>
  <!-- ChartJS -->
  <script src="assets/js/chartjs/Chart.js"></script>
  <script src="assets/js/home/js/index.js" charset="utf-8"></script>

</body>
