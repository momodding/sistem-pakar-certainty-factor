<!--============= Navbar =============-->
<nav class="navbar navbar-custom navbar-fixed-top">
  <div class="container">
    <div class="navbar-header page-scroll">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
              </button>
      <a class="navbar-brand" href="#">Sistem Pakar CF</a>
    </div>
    <div id="navbar" class="collapse navbar-collapse navbar-right navbar-main-collapse">
      <ul class="nav navbar-nav">
        <li class="hidden">
          <a href="#page-top"></a>
        </li>
        <li class="page-scroll">
          <a href="index.php">Home</a>
        </li>
        <li class="page-scroll">
          <a href="index2.php">Hitungan Manual</a>
        </li>
        <li class="page-scroll">
          <a href="#contact">Login</a>
        </li>
      </ul>
    </div>
    <!--/.nav-collapse -->
  </div>
  </nav>
    <!-- /.============= Navbar =============-->
