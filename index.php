<?php
require 'setting/koneksi.php';
$sql = "SELECT * FROM gejala";
$result = $conn->query($sql);
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>SiPakar Penyakit Sapi</title>
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link rel="stylesheet" href="assets/css/bootstrap/bootstrap.min.css">
  <link rel="stylesheet" href="assets/css/iCheck/all.css">
  <link rel="stylesheet" href="assets/css/home/css/style.css">
</head>
<body>
  <?php include 'menu.php'; ?>
  <div class="container main-box">
    <div class="card card-1">
      <div class="page-header">
        <h2>Gejala</h2>
      </div>
      <h4>Pilih gejala (minimal 3 gejala) : </h4>
      <form id="gejala-form" action="hasil.php" method="post" onsubmit="return check();">
        <div id="diagnosa">
          <?php
          if($result->num_rows > 0){
            foreach ($result as $row) {
           ?>
          <input type="checkbox" id="gejala" name="gejala[]" class="flat-blue" value="'<?php echo $row['id_gejala']; ?>'">   <?php echo $row['gejala']; ?><br><br>
        <?php }
        } ?>
        </div>
        <br>
        <button type="submit" name="tampil" class="btn btn-default">Tampilkan Hasil</button>
        <button type="button" name="reset" id="reset" class="btn btn-default" onclick="resetAll()">Reset All</button>
      </form>
    </div>
  </div>





  <?php include 'footer.php'; ?>

  <!-- jQuery 3 -->
  <script src="assets/js/jQuery/jquery.min.js"></script>
  <!-- Bootstrap 3.3.7 -->
  <script src="assets/js/bootstrap/bootstrap.min.js"></script>
  <!-- Select2 -->
  <script src="assets/js/iCheck/icheck.min.js"></script>
  <!-- Sweetalert -->
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
  <!-- InputMask -->
  <script src="assets/js/input-mask/jquery.inputmask.js"></script>
  <script src="assets/js/input-mask/jquery.inputmask.date.extensions.js"></script>
  <script src="assets/js/input-mask/jquery.inputmask.extensions.js"></script>
  <!-- FastClick -->
  <script src="assets/js/fastclick/fastclick.js"></script>
  <!-- AdminLTE App -->
  <script src="assets/js/adminlte/adminlte.min.js"></script>
  <!-- Sparkline -->
  <script src="assets/js/jquery-sparkline/jquery.sparkline.min.js"></script>
  <!-- jvectormap  -->
  <script src="assets/js/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
  <script src="assets/js/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
  <!-- SlimScroll -->
  <script src="assets/js/jquery-slimscroll/jquery.slimscroll.min.js"></script>
  <!-- ChartJS -->
  <script src="assets/js/chartjs/Chart.js"></script>
  <script src="assets/js/home/js/index.js" charset="utf-8"></script>
  <script type="text/javascript">
  	$(document).ready(function() {
      //var textarea = document.getElementById("diagnosa");
  		//textarea.scrollTop = textarea.scrollHeight;
      $('input[type="checkbox"].flat-blue').iCheck({
        checkboxClass: 'icheckbox_flat-blue'
      });
  	});
    function resetAll() {
      $('input[type="checkbox"].flat-blue').iCheck('uncheck');
    }
    function check() {
      if ($("input:checked").length < 3) {
        alert("Pilih minimal 3!!!");
        return false;
      }else {
        return true;
      }
    }
  </script>
</body>
