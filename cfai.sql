-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: 20 Des 2017 pada 16.56
-- Versi Server: 5.6.35
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cfai`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `cf_table`
--

CREATE TABLE `cf_table` (
  `id_cf` char(5) NOT NULL,
  `id_gejala` char(5) NOT NULL,
  `id_penyakit` char(5) NOT NULL,
  `mb` float NOT NULL,
  `md` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `cf_table`
--

INSERT INTO `cf_table` (`id_cf`, `id_gejala`, `id_penyakit`, `mb`, `md`) VALUES
('1', 'G001', 'P001', 0.8, 0.2),
('10', 'G015', 'P002', 0.6, 0.2),
('11', 'G001', 'P003', 0.8, 0.2),
('12', 'G002', 'P003', 0.8, 0.3),
('13', 'G004', 'P003', 0.7, 0.1),
('14', 'G009', 'P003', 0.7, 0.1),
('15', 'G010', 'P003', 0.6, 0.05),
('16', 'G017', 'P003', 0.6, 0.2),
('17', 'G001', 'P004', 0.8, 0.2),
('18', 'G002', 'P004', 0.8, 0.3),
('19', 'G005', 'P004', 0.7, 0.05),
('2', 'G003', 'P001', 0.6, 0.1),
('20', 'G016', 'P004', 0.8, 0.05),
('21', 'G014', 'P005', 0.7, 0.1),
('22', 'G015', 'P005', 0.6, 0.2),
('23', 'G016', 'P005', 0.8, 0.05),
('24', 'G002', 'P006', 0.8, 0.3),
('25', 'G003', 'P006', 0.6, 0.1),
('26', 'G010', 'P006', 0.6, 0.05),
('27', 'G017', 'P006', 0.6, 0.2),
('28', 'G002', 'P007', 0.8, 0.3),
('29', 'G012', 'P007', 0.6, 0.05),
('3', 'G004', 'P001', 0.7, 0.1),
('30', 'G019', 'P007', 0.8, 0.1),
('31', 'G020', 'P007', 0.7, 0.05),
('4', 'G005', 'P001', 0.7, 0.05),
('5', 'G002', 'P002', 0.8, 0.3),
('6', 'G006', 'P002', 0.6, 0.1),
('7', 'G007', 'P002', 0.7, 0.2),
('8', 'G008', 'P002', 0.8, 0.3),
('9', 'G014', 'P002', 0.7, 0.1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `gejala`
--

CREATE TABLE `gejala` (
  `id_gejala` char(5) NOT NULL,
  `gejala` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `gejala`
--

INSERT INTO `gejala` (`id_gejala`, `gejala`) VALUES
('G001', 'Suhu badan tinggi'),
('G002', 'Nafsu makan hilang'),
('G003', 'Diare'),
('G004', 'Darah keluar dari mulut dan lubang hidung'),
('G005', 'Kematian Mendadak'),
('G006', 'Selaput lendir didalam mulut, bibir dan gusi merah'),
('G007', 'Mulut keluar ludah seperti benang'),
('G008', 'Pergelanagan kaki dekat kuku bengkak'),
('G009', 'Bulu kelihatan rontok, kotor dan kering seperti bersisik'),
('G010', 'Menimbulkan gerakan putar-putar tanpa arah'),
('G011', 'Bengkak di beberapa tubuh'),
('G012', 'Gangguan pernapasan'),
('G013', 'Jika bengkak dipotong akan ada benda merah kotor bau busuk'),
('G014', 'Bengkak di celah kuku'),
('G015', 'Selaput kuku terkelupas'),
('G016', 'Sapi pincang dan lumpuh'),
('G017', 'Berat badan berkurang'),
('G018', 'Busung pada berbagai bagian tubuh'),
('G019', 'Lambung sebelah kiri atas membesar dan kencang'),
('G020', 'Bagian itu bila dipikul seperti drum');

-- --------------------------------------------------------

--
-- Struktur dari tabel `penyakit`
--

CREATE TABLE `penyakit` (
  `id_penyakit` char(5) NOT NULL,
  `nama` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `penyakit`
--

INSERT INTO `penyakit` (`id_penyakit`, `nama`) VALUES
('P001', 'Anthrax (Radang limpa)'),
('P002', 'Penyakit Mulut & Kuku'),
('P003', 'Surra (Penyakit Tujuh Keliling)'),
('P004', 'Blakled (Radang Paha)'),
('P005', 'Kuku Busuk'),
('P006', 'Cacing Hati'),
('P007', 'Bloat');

-- --------------------------------------------------------

--
-- Struktur dari tabel `solusi`
--

CREATE TABLE `solusi` (
  `id_solusi` int(11) NOT NULL,
  `id_penyakit` char(4) NOT NULL,
  `solusi` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `solusi`
--

INSERT INTO `solusi` (`id_solusi`, `id_penyakit`, `solusi`) VALUES
(1, 'P001', 'Semua bangkai harus dibakar,begitu pula peralatan yang habis di pakai'),
(2, 'P001', 'Daging sapi yang menderita penyakit antraxs tidak boleh di konsumsi'),
(3, 'P001', 'Pengobatan dilakukan dengan antibiotik'),
(4, 'P002', 'Membersihkan celah kuku dengan cara merendamnya kedalam cairan copper sulphate 3% atau larutan formalin 10%'),
(5, 'P002', 'Pengobatan dengan injeksi sulfa atau antibiotik, selama pengobatan kaki harus di jaga dalam keadaan kering'),
(6, 'P002', 'Pembasmian penyakit terutama di tunjukan kepada pembasmian siput(bekicot), misalnya tidak membiarkan lapangan pangonan tergenang air atau drainase jelek'),
(7, 'P002', 'Pengobatan penderita dengan Hexaclorophene'),
(8, 'P002', 'Sekali-kali jangan ngangon pada pagi hari yang rumputnya masih basah karena embun dan air hujan'),
(9, 'P002', 'Berikan pakan pendahuluan berupa jerami kering kepada sapi yang lapar sebelum di angon'),
(10, 'P003', 'Sapi yang mati akibat penyakit surra harus di bakar atau dikubur'),
(11, 'P003', 'Pengobatannya menggunakan nagonal, Arsokol, Atoxyl dan lain-lain'),
(12, 'P003', 'Jika di suatu daerah dipastikan telah terjangkit penyakit ini, maka semua sapi yang sehat harus di evakuasi'),
(13, 'P003', 'Sapi-sapi yang bisa di diagnosa secara awal secepatnya dilakukan dengan pengobatan antibiotik sebab penyakit ini berkembang begitu cepat'),
(14, 'P004', 'Kandang dan semua peralatan di upayakan selalu bersih di cuci dengan caustic soda 2%'),
(15, 'P004', 'Pengobatan dilakukan dengan injeksi antibiotik atau sulfa'),
(16, 'P004', 'penyemprotan dilakukan terhadap semua peralatan ataupun lingkungan yang banyak di hinggapi oleh lalat'),
(17, 'P004', 'Pengobatan dengan injeksi sulfa atau antibiotik, selama pengobatan kaki harus di jaga dalam keadaan kering'),
(18, 'P005', 'Kandang dan semua peralatan di upayakan selalu bersih di cuci dengan caustic soda 2%'),
(19, 'P005', 'Membersihkan celah kuku dengan cara merendamnya kedalam cairan copper sulphate 3% atau larutan formalin 10%'),
(20, 'P005', 'Pengobatan dengan injeksi sulfa atau antibiotik, selama pengobatan kaki harus di jaga dalam keadaan kering'),
(21, 'P006', 'Pembasmian penyakit terutama di tunjukan kepada pembasmian siput(bekicot), misalnya tidak membiarkan lapangan pangonan tergenang air atau drainase jelek'),
(22, 'P006', 'Pengobatan penderita dengan Hexaclorophene'),
(23, 'P006', 'Sekali-kali jangan ngangon pada pagi hari yang rumputnya masih basah karena embun dan air hujan'),
(24, 'P007', 'Berikan pakan pendahuluan berupa jerami kering kepada sapi yang lapar sebelum di angon'),
(25, 'P007', 'Penderita diberi minyak yang berasal dari tumbuh-tumbuhan, misalnya minyak kacang tanah sebanyak 0,6 liter'),
(26, 'P007', 'Jika keadan penderita sudah parah, gas di upayakan bisa keluar secepatnya yakni dengan cara menusuk perut sebelah kiri dengan trocar dan cannula');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cf_table`
--
ALTER TABLE `cf_table`
  ADD PRIMARY KEY (`id_cf`),
  ADD KEY `id_gejala` (`id_gejala`),
  ADD KEY `id_penyakit` (`id_penyakit`);

--
-- Indexes for table `gejala`
--
ALTER TABLE `gejala`
  ADD PRIMARY KEY (`id_gejala`);

--
-- Indexes for table `penyakit`
--
ALTER TABLE `penyakit`
  ADD PRIMARY KEY (`id_penyakit`);

--
-- Indexes for table `solusi`
--
ALTER TABLE `solusi`
  ADD PRIMARY KEY (`id_solusi`),
  ADD KEY `id_penyakit` (`id_penyakit`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `solusi`
--
ALTER TABLE `solusi`
  MODIFY `id_solusi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `cf_table`
--
ALTER TABLE `cf_table`
  ADD CONSTRAINT `cf_table_ibfk_1` FOREIGN KEY (`id_gejala`) REFERENCES `gejala` (`id_gejala`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `cf_table_ibfk_2` FOREIGN KEY (`id_penyakit`) REFERENCES `penyakit` (`id_penyakit`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `solusi`
--
ALTER TABLE `solusi`
  ADD CONSTRAINT `solusi_ibfk_1` FOREIGN KEY (`id_penyakit`) REFERENCES `penyakit` (`id_penyakit`) ON DELETE NO ACTION ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
