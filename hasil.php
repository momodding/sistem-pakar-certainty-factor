<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>SiPakar Penyakit Sapi</title>
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link rel="stylesheet" href="assets/css/bootstrap/bootstrap.min.css">
  <link rel="stylesheet" href="assets/css/iCheck/all.css">
  <link rel="stylesheet" href="assets/css/home/css/style.css">
</head>
<body>
  <?php include 'menu.php'; ?>
  <div class="container main-box">
    <div class="card card-1">
      <div class="page-header">
        <h2>Hasil Konsultasi</h2>
      </div>
      <?php
      require 'setting/koneksi.php';
        if (isset($_POST['tampil'])) {
          $gejala = $_POST['gejala'];
      ?>
      <div class="row">
        <div class="col-md-12">
          <div class="form-group">
            <label>Gejala Dipilih</label>
            <textarea name="gejala-dipilih" class="form-control" rows="3" readonly><?php
              $sql = "SELECT gejala FROM gejala WHERE id_gejala IN (".implode(',',$gejala).")";
              $result = $conn->query($sql);
              if($result->num_rows > 0){
                $no = 1;
                foreach ($result as $key) {
                  echo $no.".&nbsp;".$key['gejala']."&#13;&#10;";
                  $no += 1;
                }
              }
             ?></textarea>
          </div>
          <?php
            $sql = "SELECT id_penyakit FROM cf_table WHERE id_gejala IN(".implode(',',$gejala).") GROUP BY id_penyakit ORDER BY id_penyakit";


            $result = $conn->query($sql);
            if($result->num_rows > 0){
              while($row=$result->fetch_assoc()){
                $penyakit[] =  "'".$row['id_penyakit']."'";
              }
            }

            for ($i=0; $i < count($penyakit); $i++) {
              unset($gejalanya);
              unset($mb);
              unset($md);
              $sql = "SELECT id_penyakit, mb, md, id_gejala FROM cf_table WHERE id_gejala IN(".implode(',',$gejala).") AND id_penyakit = ".$penyakit[$i];
              $result = $conn->query($sql);
              $k = $i+1;
              if($result->num_rows > 0){
                while($row=$result->fetch_assoc()){
                  $gejalanya[] =  $row['id_gejala'];
                  $mb[] = $row['mb'];
                  $md[] = $row['md'];
                }
                if (count($gejalanya) == '1') {
                  $mbsatu = $mb[0];
                  $mdsatu = $md[0];
                  $cfsementara[$penyakit[$i]] = $mbsatu - $mdsatu;
                }else {
                  for ($a=0; $a < count($gejalanya); $a++) {

                    $ke = $a+1;

                    if ($a == 0) {
                      $mblama = $mb[$a];

                      $mdlama = $md[$a];

                    } else {
                      $mbbaru = $mb[$a];

                      $mdbaru = $md[$a];

                      $mbsementara = $mblama + ($mbbaru*(1-$mblama));

                      $mdsementara = $mdlama + ($mdbaru*(1-$mdlama));

                      $mblama = $mbsementara;
                      $mdlama = $mdsementara;

                    }
                  }
                  $mbfinal = $mbsementara;
                  $mdfinal = $mdsementara;
                  $cfsementara[$penyakit[$i]] = $mbfinal - $mdfinal;
                }

              }
            }

            $solusi = max($cfsementara);

            $solusikey = array_search($solusi, $cfsementara);

            $sql = "SELECT nama FROM penyakit WHERE id_penyakit =".$solusikey;
            $result = $conn->query($sql);
          ?>
          <div class="form-group">
            <label>Kemungkinan Penyakit</label>
            <input type="text" class="form-control" value="<?php
            if($result->num_rows > 0){
              foreach ($result as $key) {
                echo $key['nama'];
              }
            }
            ?>" readonly>
          </div>
          <div class="form-group">
            <label>Solusi</label>
            <textarea name="solusi-dipilih" class="form-control" rows="5" readonly><?php
              $sql = "SELECT solusi FROM solusi WHERE id_penyakit = ".$solusikey;
              $result = $conn->query($sql);
              if($result->num_rows > 0){
                $no = 1;
                foreach ($result as $key) {
                  echo $no.".&nbsp;".$key['solusi']."&#13;&#10;";
                  $no += 1;
                }
              }
             ?></textarea>
          </div>
        </div>
        <?php } ?>
        </div>
      </div>
    </div>
  </div>





  <?php include 'footer.php'; ?>

  <!-- jQuery 3 -->
  <script src="assets/js/jQuery/jquery.min.js"></script>
  <!-- Bootstrap 3.3.7 -->
  <script src="assets/js/bootstrap/bootstrap.min.js"></script>
  <!-- Select2 -->
  <script src="assets/js/iCheck/icheck.min.js"></script>
  <!-- Sweetalert -->
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
  <!-- InputMask -->
  <script src="assets/js/input-mask/jquery.inputmask.js"></script>
  <script src="assets/js/input-mask/jquery.inputmask.date.extensions.js"></script>
  <script src="assets/js/input-mask/jquery.inputmask.extensions.js"></script>
  <!-- FastClick -->
  <script src="assets/js/fastclick/fastclick.js"></script>
  <!-- AdminLTE App -->
  <script src="assets/js/adminlte/adminlte.min.js"></script>
  <!-- Sparkline -->
  <script src="assets/js/jquery-sparkline/jquery.sparkline.min.js"></script>
  <!-- jvectormap  -->
  <script src="assets/js/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
  <script src="assets/js/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
  <!-- SlimScroll -->
  <script src="assets/js/jquery-slimscroll/jquery.slimscroll.min.js"></script>
  <!-- ChartJS -->
  <script src="assets/js/chartjs/Chart.js"></script>
  <script src="assets/js/home/js/index.js" charset="utf-8"></script>

</body>
