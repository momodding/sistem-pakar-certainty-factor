<!-- Footer -->

  <div class="myfooter-bottom">
      <div class="container">
          <p class="pull-left"> Copyright © paperplain project's. All right reserved. </p>
          <div class="pull-right">
              <ul class="nav nav-pills payments">
                  <li><i class="fa fa-facebook-square"></i>&nbsp;</li>
                  <li><i class="fa fa-twitter"></i>&nbsp;</li>
                  <li><i class="fa fa-instagram"></i></li>
              </ul>
          </div>
      </div>
  </div>
  <!--/.footer-bottom-->
  <!-- End Footer -->
